#include <iostream>
#include "BufferList.h"

int main() {
	BufferList *BL = new BufferList();
	BufferList::BufferListIterator itr(BL);
	BL->addElem(1, itr);
	itr.next();
	BL->addElem(3, itr);
	BL->addElem(2, itr);
	itr.start();
	std::cout << "BL elements : ";
	while (!itr.finish()) {
		itr.next();
		std::cout << itr.getElement() << " ";
	}
	std::cout  << std::endl << std::endl;
	Iterator *itr2 = BL->getIterator();
	itr2->next();

	std::cout << "BL first element  :";

	std::cout << itr2->getElement() << std::endl;
	Iterator *itr3 = BL->findElem(3);
	BL->deleteElem(*itr3);
	itr.start();
	std::cout << std::endl << std::endl;
	std::cout << "BL after deleting  element :";
	while (!itr.finish()) {
		itr.next();
		std::cout << itr.getElement() << " ";
	}
	std::cout << std::endl << std::endl;
	std::cout << "Size: " << BL->getSize() << std::endl;
	std::cout << "isEmpty: " << BL->isEmpty() << std::endl;
	BL->makeEmpty();
	std::cout << "isEmpty: " << BL->isEmpty() << std::endl;
	itr.start();
	std::cout << std::endl << std::endl;
	while (!itr.finish()) {
		itr.next();
		std::cout << itr.getElement() << std::endl;
	}
	system("pause");
	return 0;
}